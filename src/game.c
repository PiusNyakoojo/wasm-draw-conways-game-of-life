#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <emscripten.h>

EMSCRIPTEN_KEEPALIVE
void update_grid(uint8_t grid[], int n_rows, int n_columns) {
  // Update the world representation grid for Conway's Game of Life.
  // The grid is composed of cells that are either dead or alive. Our goal is to
  // determine what state they will be in the next-time step. Whether any particular
  // cell is dead or alive will depend on the state of their 8 closest neighbors.
  //
  // We update the grid for a single time step. To run a continuous simulation,
  // call this function repeatedly.
  //
  // :param grid:
  // The 2-dimensional grid representing the cellular automata world. The contents
  // of the grid represent the state of the cell at that position. A cell is either
  // dead (represented as a 0) or alive (represented as a 1).
  //
  // For example, pretend the value at position (0, 0) of our grid is 1. We interpret
  // this as the cell at position (0, 0) being alive.
  //
  // The grid is treated as a wrapped-world grid, that is, the world wraps in on itself at
  // the edges. In other words, the left edge of the grid is connected to the right edge
  // and the top edge is connected to the bottom edge. You can imagine the entire
  // simulation existing on the surface of a torus (also known as a donut).
  // What goes beyond the right edge will return from the left edge. What goes beyond
  // the top edge will return from the bottom edge.
  //
  // :param n_rows:
  // The number of rows of the grid.
  //
  // :param n_columns:
  // The number of columns of the grid.

  // Create a new grid so we don't override values on the old grid until our
  // calculation of the new world is complete.
  uint8_t new_grid[n_rows * n_columns];

  // The following for-loop will iterate through all the cells of the grid and determine
  // whether that cell is alive or dead in the new grid. The condition of whether the cell
  // is alive or dead depends on the following 4 rules:
  // 1. Any live cell with fewer than 2 live neighbors dies, as if caused by underpopulation.
  // 2. Any live cell with 2 or 3 live neighbors lives on in the next generation.
  // 3. Any live cell with more than 3 live neighbors dies, as if by overpopulation.
  // 4. Any dead cell with exactly 3 live neighbors becomes a live cell, as if by reproduction.

  // Initialize our row and column variables.
  int previous_row, previous_column;
  int row, column;
  int next_row, next_column;

  // The current cell in the iteration is at the position (row, column) of our grid.
  // Our current cell is always in the center (with respect to its neighbors).
  // The neighboring cells of our current cell is represented by the following map:
  //
  // [top_left, top_center, top_right]
  // [center_left, current_cell, center_right]
  // [bottom_left, bottom_center, bottom_right]

  // Initialize the states of our neighboring cells and the current cell.
  int top_left = 0, top_center = 0, top_right = 0;
  int center_left = 0, current_cell = 0, center_right = 0;
  int bottom_left = 0, bottom_center = 0, bottom_right = 0;

  // Initialize the number of living neighboring cells.
  //
  // Because we chose 0 and 1 as an encoding scheme for whether a cell is
  // dead or alive, respectively, adding the neighboring cells together
  // results in the number of living neighboring cells.
  int n_living_neighbors = 0;

  // Process the original grid to determine our new grid.
  int i;

  for (i = 0; i < n_rows * n_columns; i++) {
    // Calculate the current row, previous row and next row.
    row = i / n_columns;
    previous_row = row - 1, next_row = row + 1;

    // When the previous row is less than the first possible row (which is 0),
    // wrap back to the last row (n_rows - 1). This allows us to wrap the world
    // around along the vertical axis (the row axis).
    if (previous_row < 0)
      previous_row = n_rows - 1;

    // Similarly, we need to ensure that the world is wrapped back to the top-most row
    // when we exceed the number of rows.
    if (next_row > n_rows - 1)
      next_row = 0;

    // Calculate the current column, previous column and next column.
    column = i % n_columns;
    previous_column = column - 1, next_column = column + 1;

    // When the previous column is less than the first possible column (0), wrap back
    // to the last column (n_columns - 1). This allows us to wrap the world around
    // along the horizontal axis (the column axis).
    if (previous_column < 0)
      previous_column = n_columns - 1;

    // Similarly, we need to ensure that the world is wrapped back to the left-most
    // column when we exceed the number of columns.
    if (next_column > n_columns - 1)
      next_column = 0;

    // We need to know whether the current cell is
    // dead or alive to determine the rule to apply for its next state.
    current_cell = grid[row * n_columns + column];

    // Get the state of the neighboring cells.
    top_left = grid[previous_row * n_columns + previous_column];
    top_center = grid[previous_row * n_columns + column];
    top_right = grid[previous_row * n_columns + next_column];
    center_left = grid[row * n_columns + previous_column];
    center_right = grid[row * n_columns + next_column];
    bottom_left = grid[next_row * n_columns + previous_column];
    bottom_center = grid[next_row * n_columns + column];
    bottom_right = grid[next_row * n_columns + next_column];

    // Add the states of all our neighboring cells to determine the number
    // of living neighbors.
    n_living_neighbors = top_left + top_center + top_right;
    n_living_neighbors += center_left + center_right;
    n_living_neighbors += bottom_left + bottom_center + bottom_right;

    // Apply the rules for when the cell is alive (when the cell is 1).
    if (current_cell == 1) {
      // 1. The current cell dies as if by underpopulation.
      if (n_living_neighbors < 2) {
        new_grid[row * n_columns + column] = 0;

      // 2. The current cell remains alive.
      } else if (n_living_neighbors == 2 || n_living_neighbors == 3) {
        new_grid[row * n_columns + column] = 1;

      // 3. The current cell dies as if by overpopulation.
      } else if (n_living_neighbors > 3) {
        new_grid[row * n_columns + column] = 0;
      }

    // Apply the rules for when the cell is dead (when the cell is 0).
    } else if (current_cell == 0) {
      // 4. The current cell is brought to life as if by reproduction.
      if (n_living_neighbors == 3) {
        new_grid[row * n_columns + column] = 1;
      }
    }
  }

  // Copy the contents of our new grid into the original grid.
  memcpy(grid, new_grid, sizeof(new_grid));

  // Free the memory required for our new grid after having copied it over to our
  // original grid in the previous line.
  free(new_grid);
}
