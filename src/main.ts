import { prepareCWrapFunctions, updateGrid } from './util/cwrap'

const WASM_FILE_NAME = 'a.out'

// tslint:disable-next-line
declare let Module: any

(<any>window).Module = {
  preRun: [],
  postRun: [],
  wasmBinaryFile: `${WASM_FILE_NAME}.wasm`,
  onRuntimeInitialized: init
}

// Initialize the program environment and prepare for runtime.
function init () {
  prepareCWrapFunctions()

  // Run the main program.
  main()
}

// Run the program.
function main () {
  // "main" is the starting point for our program. After our program's environment
  // is prepared, "main" is called.
  //
  // The goal of this program is to demo the use of WebAssembly.
  // We will demo the WebAssembly technology using the well-known application,
  // "Conway's Game of Life."
  //
  // Conway's Game of Life is a world that operates on a few simple rules
  // and yet produces amazingly intricate and beautiful behaviors. 🌍


  // To draw this living world to the computer screen, we will need to prepare
  // an HTML5 Canvas element to receive our painting.
  const canvas = <HTMLCanvasElement>document.getElementById('canvas')
  const ctx = canvas.getContext('2d')

  // We will use the height and width of our canvas to know how large to make
  // the image we want to paint to the canvas.
  const width = canvas.width
  const height = canvas.height

  // We will give data to this image object and pass it to our canvas later
  // when we want to draw the image to the canvas.
  //
  // The image data is structured like an array. Each element of the array
  // corresponds to a piece of our canvas. Drawing all of these pieces together
  // will render our image to the screen.
  const image = ctx.createImageData(width, height)

  // A buffer is a set amount of space where we can place things.
  // Think of it like an apartment room somewhere in the computer.
  // We can send data to the apartment, for example our image data. 🖼️
  //
  // We need to create a buffer that will store our image data.
  // Doing so will allow our WebAssembly language of choice - which is C -
  // to communicate with our JavaScript code.
  //
  // We need to communicate between languages so our C code (the code that will
  // be compiled to WebAssembly) can do all the computationally expensive
  // things that our JavaScript code isn't well equipped for.

  // The size of our buffer (for example, the aparment room size) should be large
  // enough to store our image data.
  const BUFFER_SIZE = width * height

  // We need a pointer to our buffer space. A pointer is an address space
  // (e.g. the address for your apartment room) that we can use to know
  // where in the computer's memory our data is.
  const BUFFER_POINTER = Module._malloc(BUFFER_SIZE)

  // We now know that our pointer (the address for our apartment room) is the
  // address to a room with enough space for our image data.
  //
  // We now need to set the pointer in our shared memory space so that our
  // C code can know what memory space (the city where our apartment is
  // located within the computer) to use when looking for the buffer.
  Module._memset(BUFFER_POINTER, 0, BUFFER_SIZE)

  // Finally, let's actually create our buffer so we can give tell it
  // what our image looks like.
  //
  // Our image will be black and white so we will use Uint8Array to give
  // us an array-like view into the underlying HEAP buffer that the Module
  // object provides us.
  //
  // The Uint in Uint8 denotes we are using unsigned integer values (e.g. no
  // negative integers like -1, -2, etc.)
  //
  // The 8 in Uint8 denotes our use of 8-bit values for each element of the array.
  // 8-bits gives us 2^8 = 256 numbers to work with. In other words, each element
  // of our buffer will be a number between 0 to 255 (inclusive).
  //
  // Using our apartment analogy from earlier, we can see that our buffer (our
  // apartment) is exactly the size needed for our image. No greater, no less.
  // So much for moving in..
  let buffer = new Uint8Array(Module.HEAPU8.buffer, BUFFER_POINTER, BUFFER_SIZE)

  // Now is the time to think about our world again. Recall that we are trying to
  // draw the Conway's Game of Life world.
  //
  // To run the world, we need to start with an initial state and see how the world
  // evolves from this initial state.
  //
  // An empty initial state is kind of boring to watch so let's try something more
  // interesting: randomizing the state of each cell in the world. In other words,
  // for each of the cells in our world, we will randomly decide if it is dead or alive.
  //
  // We will let 0 mean the cell is dead and 1 mean the cell is alive.
  //
  // Recall that Math.round rounds to the nearest integer and that Math.random generates
  // a random decimal value between 0 and 1 (exclusive).
  //
  // The following should set each cell of our array (which is the space which
  // constains our image data) to a value of 0 or 1.
  for (let i = 0; i < buffer.length; i++) {
    buffer[i] = Math.round(Math.random())
  }

  // Our image data is stored in a buffer. We've initialized our buffer with
  // the initial data for our image.
  //
  // We can now start animating our world to see what happens! 🎉🥂
  //
  // However, drawing our world isn't as easy as calling a function once.
  // Animation is a continual thing, like a for-loop. We need to call a function
  // multiple times.
  //
  // To call a function repeatedly, we can use setTimeout so our draw function is
  // called once every 1000 milliseconds.

  // We will set our animation tick rate to 100 milliseconds (0.1 second).
  // Play around with this value to see what happens!
  const TICK_RATE = 100

  // Define what happens during animation of the world.
  const animate = function () {
    setTimeout(function () {
      // Update the world grid of Conway's Game of Life.
      updateGrid(buffer.byteOffset, height, width)

      // After updating the world grid, we need to update our image
      // with the new world grid data. Our goal is to draw this image.
      //
      // To do this we can loop through the buffer and set up our
      // image data. This will make drawing the image to the canvas
      // very easy using the ctx.putImageData method.
      for (let i = 0; i < BUFFER_SIZE; i++) {
        const color = buffer[i] ? 0 : 0xFF
        image.data[i * 4] = color
        image.data[i * 4 + 1] = color
        image.data[i * 4 + 2] = color
        image.data[i * 4 + 3] = 0xFF
      }

      // Draw the image data to our canvas using its context.
      ctx.putImageData(image, 0, 0)

      // Re-animate the canvas.
      animate()
    }, TICK_RATE)
  }

  // Animate Conway's Game of Life! 🌍
  animate()
}
