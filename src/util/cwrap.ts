declare let Module: any

let cwrapFunctions: { [key: string]: Function } = {}

function prepareCWrapFunctions () {
  cwrapFunctions['updateGrid'] = Module.cwrap('update_grid', null, ['number', 'number', 'number'])
  // cwrapFunctions['helloWorld'] = Module.cwrap('hello_world', null, [])
}

function updateGrid (gridPtr: number, nRows: number, nColumns: number): any {
  return cwrapFunctions['updateGrid'](gridPtr, nRows, nColumns)
}

function helloWorld (...args: any[]): any {
  return cwrapFunctions['helloWorld'](...args)
}

export {
  prepareCWrapFunctions,
  updateGrid,
  helloWorld
}
