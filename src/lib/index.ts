// Declare global variables for the typescript compiler.
// mergeInto and LibraryManager are provided by emscripten.
declare var mergeInto: any
declare var LibraryManager: any

let lib = {}

mergeInto(LibraryManager.library, lib)
