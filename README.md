# 🌏 Draw Conway's Game of Life using WebAssembly w/ TypeScript support

## 💾 Installation Requirements
```bash
# Install Emscripten
# - used to compile C to WebAssembly
# - install instructions: http://kripken.github.io/emscripten-site/

# Install Node & Node Package Manager (npm)
# - used to build the project (follow instructions below)
# - install instructions: https://nodejs.org/en/
```

## ⚙️ Build Setup

```bash
# install dependencies
npm install

# build the application for distribution
npm run build

# start server at localhost:8080
npm run start

# start development server for watching files and automatic re-building
npm run dev
```

## 🔨 Tools Used
* [WebAssembly](http://webassembly.org/)
* [TypeScript](https://www.typescriptlang.org/)

## ❤️ Demo

![Image of Conway's Game of Life](https://gitlab.com/PiusNyakoojo/wasm-draw-conways-game-of-life/raw/master/doc/demo.png)
