var gulp = require('gulp')
var path = require('path')
var browserSync = require('browser-sync')
var ts = require('gulp-typescript')
var gulpTslint = require('gulp-tslint')
var tslint = require('tslint')
var shell = require('gulp-shell')
var runSequence = require('run-sequence')
var replace = require('gulp-replace')
var merge = require('merge-stream')

var envConfig = require('./env')
var exportedFunctions = require('./exported_functions')

var PATH_SRC = envConfig.PATH_SRC
var PATH_DIST = envConfig.PATH_DIST
var PATH_BUILD = envConfig.PATH_BUILD

// Build project and start server
gulp.task('serve', function (done) {
  runSequence(
    'build',
    'watch',
    function () {
      done()
    }
  )
})

// Watch project files for changes
gulp.task('watch', function () {
  gulp.watch(`${PATH_SRC}/index.html`, ['copy_indexHTML'])
  gulp.watch(`${PATH_SRC}/main.ts`, ['build_js_main'])
  gulp.watch(`${PATH_SRC}/util/**.ts`, ['build_js_main'])
  gulp.watch(`${PATH_SRC}/lib/**.ts`, ['build_js_lib'])
  gulp.watch(`${PATH_SRC}/**/*.c`, ['build_wasm'])
})

// Build project
gulp.task('build', function (done) {
  runSequence(
    'clean_dist',
    'copy_indexHTML',
    'tslint',
    'typescript',
    'bundle_js_lib',
    'bundle_js_main',
    'build_wasm',
    function () {
      done()
    })
})

// Build --js-library *.js file
gulp.task('build_js_lib', function (done) {
  runSequence(
    'tslint',
    'typescript',
    'bundle_js_lib',
    'bundle_js_main',
    function () {
      done()
    }
  )
})

// Build main.js
gulp.task('build_js_main', function (done) {
  runSequence(
    'tslint',
    'typescript',
    'bundle_js_lib',
    'bundle_js_main',
    function () {
      done()
    }
  )
})

// Build .wasm
gulp.task('build_wasm',
  shell.task(`emcc '${PATH_SRC}/game.c' -o '${PATH_DIST}/a.out.js' --js-library '${PATH_DIST}/lib/index.js'${exportedFunctions.list.length == 0 ? '': ` -s EXPORTED_FUNCTIONS='[${exportedFunctions.list}]'`} -s EXTRA_EXPORTED_RUNTIME_METHODS='["ccall", "cwrap"]' -s WASM=1`)
)

// Bundle --js-library files
gulp.task('bundle_js_lib',
  shell.task(`webpack --config '${PATH_BUILD}/lib.webpack.js'`)
)

// Bundle main.js files
gulp.task('bundle_js_main',
  shell.task(`webpack --config '${PATH_BUILD}/main.webpack.js'`)
)

// Compile TypeScript (.ts) to JavaScript (.js)
gulp.task('typescript', function () {
  var tsProject = ts.createProject(`${PATH_SRC}/tsconfig.json`)
  return gulp.src([`${PATH_SRC}/**/*.ts`])
    .pipe(tsProject())
    .pipe(gulp.dest(`${PATH_DIST}`))
})

// Lint TypeScript files (.ts)
gulp.task('tslint', function () {
  var program = tslint.Linter.createProgram(`${PATH_SRC}/tsconfig.json`)

  return gulp.src([`${PATH_SRC}/**/*.ts`], { base: '.' })
    .pipe(gulpTslint({
      program,
      formatter: "verbose"
    }))
    .pipe(gulpTslint.report())
})

// Clean /dist
gulp.task('clean_dist',
  shell.task(`rm -rf ${PATH_DIST}/*`)
)

// Copy /src/index.html to /dist/
gulp.task('copy_indexHTML', function () {
  return gulp.src(`${PATH_SRC}/index.html`)
    .pipe(gulp.dest(`${PATH_DIST}`))
})
