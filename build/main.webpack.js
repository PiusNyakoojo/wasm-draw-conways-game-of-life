var path = require('path')

module.exports = {
  entry: path.resolve(__dirname, '../dist/main.js'),
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'main.js'
  }
}
